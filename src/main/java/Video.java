import lombok.Getter;
import lombok.Setter;

import java.util.Arrays;

public class Video {

    private String id;
    @Getter @Setter private int sourceId;
    @Getter @Setter private String url240;
    @Getter @Setter private String externalThumbnail;
    @Getter @Setter private String addedDate;
    @Getter @Setter private String keywords;
    @Getter @Setter private int framerate;
    @Getter @Setter private String description;
    @Getter @Setter private String language;
    @Getter @Setter private String type;
    @Getter @Setter private String title;
    @Getter @Setter private int duration;
    @Getter @Setter private int feedId;
    @Getter @Setter private int parserId;
    @Getter @Setter private int height;
    @Getter @Setter private String externalUrl;
    @Getter @Setter private String iABCategoryCodes[];
    @Getter @Setter private String thumbnail;
    @Getter @Setter private String externalId;
    @Getter @Setter private String url;
    @Getter @Setter private String keywordsArray[];
    @Getter @Setter private boolean deleted;
    @Getter @Setter private int fileSize;
    @Getter @Setter private int width;
    @Getter @Setter private String publishedDate;
    @Getter @Setter private String encoded_240p;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getSourceId() {
        return sourceId;
    }

    public void setSourceId(int sourceId) {
        this.sourceId = sourceId;
    }

    public String getUrl240() {
        return url240;
    }

    public void setUrl240(String url240) {
        this.url240 = url240;
    }

    public String getExternalThumbnail() {
        return externalThumbnail;
    }

    public void setExternalThumbnail(String externalThumbnail) {
        this.externalThumbnail = externalThumbnail;
    }

    public String getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(String addedDate) {
        this.addedDate = addedDate;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public int getFramerate() {
        return framerate;
    }

    public void setFramerate(int framerate) {
        this.framerate = framerate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getFeedId() {
        return feedId;
    }

    public void setFeedId(int feedId) {
        this.feedId = feedId;
    }

    public int getParserId() {
        return parserId;
    }

    public void setParserId(int parserId) {
        this.parserId = parserId;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getExternalUrl() {
        return externalUrl;
    }

    public void setExternalUrl(String externalUrl) {
        this.externalUrl = externalUrl;
    }

    public String[] getiABCategoryCodes() {
        return iABCategoryCodes;
    }

    public void setiABCategoryCodes(String[] iABCategoryCodes) {
        this.iABCategoryCodes = iABCategoryCodes;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String[] getKeywordsArray() {
        return keywordsArray;
    }

    public void setKeywordsArray(String[] keywordsArray) {
        this.keywordsArray = keywordsArray;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public int getFileSize() {
        return fileSize;
    }

    public void setFileSize(int fileSize) {
        this.fileSize = fileSize;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public String getPublishedDate() {
        return publishedDate;
    }

    public void setPublishedDate(String publishedDate) {
        this.publishedDate = publishedDate;
    }

    public String getEncoded_240p() {
        return encoded_240p;
    }

    public void setEncoded_240p(String encoded_240p) {
        this.encoded_240p = encoded_240p;
    }

    @Override
    public String toString() {
        return "Video{" +
                "sourceId=" + sourceId +
                ", url240='" + url240 + '\'' +
                ", externalThumbnail='" + externalThumbnail + '\'' +
                ", addedDate='" + addedDate + '\'' +
                ", keywords='" + keywords + '\'' +
                ", framerate=" + framerate +
                ", description='" + description + '\'' +
                ", language='" + language + '\'' +
                ", type='" + type + '\'' +
                ", title='" + title + '\'' +
                ", duration=" + duration +
                ", feedId=" + feedId +
                ", parserId=" + parserId +
                ", height=" + height +
                ", externalUrl='" + externalUrl + '\'' +
                ", iABCategoryCodes=" + Arrays.toString(iABCategoryCodes) +
                ", thumbnail='" + thumbnail + '\'' +
                ", externalId='" + externalId + '\'' +
                ", url='" + url + '\'' +
                ", keywordsArray=" + Arrays.toString(keywordsArray) +
                ", deleted=" + deleted +
                ", fileSize=" + fileSize +
                ", width=" + width +
                ", publishedDate='" + publishedDate + '\'' +
                ", encoded_240p='" + encoded_240p + '\'' +
                '}';
    }

    //            "sourceId": 110,
//            "url240": "https://nv.vi-serve.com/a3/nnezrn3t8ffxe1x1lgsi_240p.mp4",
//            "externalThumbnail": "https://img.bleacherreport.net/cms/media/image/dc/4b/84/e3/9501/4169/a57c/cc6cd61f30de/crop_exact_full_image.jpeg?h=2027&q=90&w=3602",
//            "addedDate": "2018-12-07T18:09:17.0501785Z",
//            "keywords": "nba,indiana pacers vs cleveland cavaliers 2018 10 8,indiana pacers,cleveland cavaliers,victor oladipo,V.Oladipo,",
//            "framerate": 0,
//            "description": "",
//            "language": "en",
//            "type": "video/mp4",
//            "title": "Victor Oladipo Highlights vs. Cleveland Cavaliers ",
//            "duration": 50,
//            "feedId": 381,
//            "parserId": 110,
//            "height": 0,
//            "externalUrl": "https://media.bleacherreport.com/video/upload/f_m3u8/sp_hd/v1539048241/nnezrn3t8ffxe1x1lgsi.m3u8",
//            "iABCategoryCodes": [
//            "IAB17"
//            ],
//            "thumbnail": "https://nv.vi-serve.com/a17/dc/4b/84/e3/9501/4169/a57c/cc6cd61f30de/crop_exact_full_image.jpeg?h=2027&q=90&w=3602",
//            "externalId": "42582",
//            "url": "https://nv.vi-serve.com/a3/nnezrn3t8ffxe1x1lgsi_720p.mp4",
//            "keywordsArray": [
//            "nba",
//            "indiana pacers vs cleveland cavaliers 2018 10 8",
//            "indiana pacers",
//            "cleveland cavaliers",
//            "victor oladipo",
//            "v.oladipo"
//            ],
//            "deleted": false,
//            "fileSize": 0,
//            "width": 0,
//            "publishedDate": "2018-10-09T04:25:35+03:00",
//            "encoded_240p": "_for_test_encoded_240p_for_test"


}
