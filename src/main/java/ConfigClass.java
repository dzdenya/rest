import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpHost;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;

import java.io.IOException;

public class ConfigClass {

    private static final String HOST = "elasticsearch-vis-dev.vi-serve.com";
    private static final int PORT_ONE = 80;
    //    private static final int PORT_TWO = 9201;

    private static final String SCHEME = "https";
    private static RestHighLevelClient restHighLevelClient;
    private static ObjectMapper objectMapper = new ObjectMapper();
    private static final String INDEX = "native_video_en";
    private static final String TYPE = "native_video";


    private static synchronized RestHighLevelClient makeConnection() {

     //   if (restHighLevelClient == null) {
            restHighLevelClient = new RestHighLevelClient(
                    RestClient.builder(
                            new HttpHost(HOST, PORT_ONE, SCHEME)));
    //    }

        return restHighLevelClient;
    }


    private static synchronized void closeConnection() throws IOException {
        restHighLevelClient.close();
        restHighLevelClient = null;
    }

    private static Video getVideoById(String id) throws IOException {
        GetRequest getVideoRequest = new GetRequest(INDEX, TYPE, id);
        GetResponse getResponse = restHighLevelClient.get(getVideoRequest, RequestOptions.DEFAULT);

        return getResponse != null ?
                objectMapper.convertValue(getResponse.getSourceAsMap(), Video.class) : null;
    }

    public static void main(String[] args) throws IOException {
        makeConnection();
        Video video = new Video();
        System.out.println("Getting 110_42622...");
        Video videoFromElastic = getVideoById("110_42622");
        System.out.println("Video from Elastic  --> " + videoFromElastic);

        closeConnection();
    }
}









    
