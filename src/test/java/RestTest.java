import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;
import io.restassured.response.Response;

import static io.restassured.RestAssured.given;

public class RestTest {

    public static Response doGetRequest(String endpoint) {
        RestAssured.defaultParser = Parser.JSON;

        return
                given().headers("Content-Type", ContentType.JSON, "Accept", ContentType.JSON).
                        when().get(endpoint).
                        then().contentType(ContentType.JSON).extract().response();
    }

    public static void main(String[] args) {
//        Response response = doGetRequest("https://jsonplaceholder.typicode.com/users");
        Response response = doGetRequest("https://vis-dev.vi-serve.com/playlist?url=https://indianexpress.com/article/world/pope-francis-says-china-talks-going-well-dialogue-worth-the-risk-5227094/&category=IAB8&publisherid=888&keywords=&language=en-en&mobile=true&tracing=true&skipVideoCache=true");


//        String jsonResponse = response.jsonPath().getString("$");
        String jsonResponse = response.jsonPath().getString("traceInfo.Publisher ID");
        System.out.println(jsonResponse);


//        System.out.println(fullJobj);

//        System.out.println(jsonResponse.size());

    }
}