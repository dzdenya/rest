import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.get;
import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.hasItems;

public class GetCodeStatus {

    private String RESTAPI = "https://manage-api-vis-dev.vi-serve.com/word-blacklist/publisher/112/en";
    private String RESTAPI1 = "https://vis-dev.vi-serve.com/playlist?url=https://indianexpress.com/article/world/pope-francis-says-china-talks-going-well-dialogue-worth-the-risk-5227094/&category=IAB8&publisherid=888&keywords=&language=en-en&mobile=true";


    @Test()
    public void getStatus() {

        Response assured = get(RESTAPI);

        int getCode = assured.getStatusCode();

        Assert.assertEquals(200, getCode);
    }

    @Test
    public void getStatus1() {
        given().get(RESTAPI)
                .then().statusCode(200);
    }


    String array[] = {"'test'"};

    @Test
    public void getBody1() {
        given().get(RESTAPI)
                .then().body("$", hasItems(array));
    }


    @Test()
    public void getBody() {

        Response assured = get(RESTAPI);

        String body = assured.body().print();


        Assert.assertEquals("[\"'test'\"]", body);
    }
}
