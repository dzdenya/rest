    public class EndPoint {

       public static String MANAGE_API = "https://manage-api-vis-dev.vi-serve.com";
       public static String WORD_BLACKLIST_PUBLISHER_112_EN = MANAGE_API + "/word-blacklist/publisher/112/en";



        public static String GET_EMPLOYEE = "/employee/getEmployee";

        public static String GET_ALL="/employee/getAll";
        public static String GET_EMPLOYEE_PATH_PARAM ="/employee/getEmployee/{employeeId}";
        public static String GET_EMPLOYEE_QUERY_PARAM = "/employee/getEmployeeQuery";

       public static String GOOGLE_API = "/v1/volumes";
    }

