import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.hasItems;

public class PublisherWordsWhitelist {


    private String manageApi = "https://manage-api-vis-dev.vi-serve.com/word-whitelist/publisher/113/it";

    String arrayWord[] = {"'Gattuso'"};

    //Add word to Blacklist for publisherID:113

    @Test()
//   public void addWord(word = "Gattuso"){
    public void addWord() {
        given().post(EndPoint.MANAGE_API + "?word=Gattuso")
                .then().statusCode(202);
    }


    //Check that word was added to Blacklist for publisherID:122

    @Test(dependsOnMethods = {"addWord"})
    public void getBody() {
        given().get(manageApi)
                .then().statusCode(200)
                .body("$", hasItems(arrayWord));
    }


    //Delete word from Blacklist for publisherID:113
//   public void deleteWord(word = "Gattuso"){
    @Test(dependsOnMethods = {"getBody"})
    public void deleteWord() {

        given().delete(manageApi + "?word=Gattuso")
                .then().statusCode(202);
//                    .body("$", hasItems(arrayWord));
    }
}


//    @Test()
//    public void addWord(){
//
//         response = manageApi.given().when().queryParam("word", "Kardashian");
//
//        int getStatusCode = assured.getStatusCode();
//
//        Assert.assertEquals(202, getStatusCode);
//    }

//    @Test
//    public void whenUsePathParam_thenOK() {
//        given().pathParam(new word. "eugenp")
//                .when().get("/words/{word}/repos")
//                .then().statusCode(200);
//    }
//
//
//    //http://localhost:8080/SprintRestServices/employee/addEmployee
//    @Test(groups = "demo")
//    public void validateFormParameters() {
//        RequestSpecification request = new RestAssuredConfiguration().getRequestSpecification();
//        requestSpecification.accept(ContentType.JSON).formParam("word", 100).log().all();
//        given().spec(requestSpecification).post(EndPoint.WORD_BLACKLIST_PUBLISHER_112_EN).
//                then().statusCode(200).log().all();
//    }


