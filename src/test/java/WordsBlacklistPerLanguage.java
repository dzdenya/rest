import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.hasItems;

public class WordsBlacklistPerLanguage {


    private String manageApiWordBlacklist = "https://manage-api-vis-dev.vi-serve.com/word-blacklist/de";

    String arrayWord[] = {"lesbian"};


    @Test()
//   public void addWord(word = "lesbian"){
    public void addWord() {
        given().post(manageApiWordBlacklist + "?word=lesbian")
                .then().statusCode(202);
    }


    @Test(dependsOnMethods = {"addWord"})
    public void getBody() {
        given().get(manageApiWordBlacklist)
                .then().statusCode(200)
                .body("$", hasItems(arrayWord));
    }


    //Delete word from Blacklist for publisherID:122
//   public void deleteWord(word = "Kardashian"){
    @Test(dependsOnMethods = {"getBody"})
    public void deleteWord() {
        given().delete(manageApiWordBlacklist + "?word=lesbian")
                .then().statusCode(202);
//                    .body("$", hasItems(arrayWord));
    }
}
