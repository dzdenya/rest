import io.restassured.response.Response;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.*;
import static org.hamcrest.CoreMatchers.hasItems;

public class PublisherWordsBlacklist {


    private String manageApi = "https://manage-api-vis-dev.vi-serve.com/word-blacklist/publisher/112/en";

    String arrayWord[] = {"'Kardashian'"};

    //Add word to Blacklist for publisherID:122

    @Test()
//   public void addWord(word = "Kardashian"){
    public void addWord() {
         given().param("word", "Kardashian").when().post(manageApi)
                .then().statusCode(202);
    }


    //Check that word was added to Blacklist for publisherID:122

    @Test(dependsOnMethods = {"addWord"})
    public void getBody() {
        given().get(manageApi)
                .then().statusCode(200)
                .body("$", hasItems(arrayWord));
    }


    //Delete word from Blacklist for publisherID:122
//   public void deleteWord(word = "Kardashian"){
    @Test(dependsOnMethods = {"getBody"})
    public void deleteWord() {
        given().delete(manageApi + "?word=Kardashian")
                .then().statusCode(202);
//                    .body("$", hasItems(arrayWord));
    }
}


//    @Test()
//    public void addWord(){
//
//         response = manageApi.given().when().queryParam("word", "Kardashian");
//
//        int getStatusCode = assured.getStatusCode();
//
//        Assert.assertEquals(202, getStatusCode);
//    }

//    @Test
//    public void whenUsePathParam_thenOK() {
//        given().pathParam(new word. "eugenp")
//                .when().get("/words/{word}/repos")
//                .then().statusCode(200);
//    }
//
//
//    //http://localhost:8080/SprintRestServices/employee/addEmployee
//    @Test(groups = "demo")
//    public void validateFormParameters() {
//        RequestSpecification request = new RestAssuredConfiguration().getRequestSpecification();
//        requestSpecification.accept(ContentType.JSON).formParam("word", 100).log().all();
//        given().spec(requestSpecification).post(EndPoint.WORD_BLACKLIST_PUBLISHER_112_EN).
//                then().statusCode(200).log().all();
//    }


